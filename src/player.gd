extends KinematicBody2D

var timer = Timer.new()

var pressed = false
var time_left = 0.0
var rotation_dir = 0
onready var audio_player = get_node("../AudioStreamPlayer2D")

func _ready():
	add_child(timer)
	timer.set_wait_time(60 / 130)
	timer.connect("timeout", self, "step")
	timer.start()
	
func step():
	var collision = move_and_collide(Vector2(0, -64).rotated(rotation), true, true, true)
	if not audio_player.playing:
		audio_player.playing = true
	
	if pressed:
		var rot = rotation_dir * PI/2 + rotation
		
		rotation_dir = 0
		pressed = 0
		
		print(time_left)
		
		var tween = $Tween
		tween.playback_process_mode = 0
		tween.interpolate_property(self, "rotation", rotation, rot, 0.25, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		tween.start()

	elif collision != null:
		if collision.collider == get_parent().get_node("Tiles/Goal"):
			print("WIN")
			move_local_y(-64)
		else:
			print("END")
		timer.stop()
	else:
		move_local_y(-64)

func _input(event):
	if event is InputEventKey:
		if event.is_pressed() and not pressed:
			if event.scancode == KEY_LEFT:
				rotation_dir -= 1
				pressed = true
				time_left = timer.time_left * 60 / 130
			elif event.scancode == KEY_RIGHT:
				rotation_dir += 1
				pressed = true
				time_left = timer.time_left * 60 / 130

